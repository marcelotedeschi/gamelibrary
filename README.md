# README #

Esse readme contém instruções para rodar o App GameLibrary

###

### Utilizando o serviço de maquina virtual para testes Cloud9 ###

Para visualizar a aplicação rodando, basta acessar esse link publico:

* https://gamelibrary-marcelotedeschi.c9.io

Para visualizar o código no editor do cloud9 basta criar uma conta em https://c9.io/ e acessar esse link:

* https://ide.c9.io/marcelotedeschi/gamelibrary

Se quiser editar e rodar na maquina virtual, só enviar um pedido para colaborar que eu aceito!

### Rodando o App localmente ###

1o Passo: Clone do Bitbucket

2o Passo: Instalar Rails

* http://installrails.com/

3o Passo: Instalar Ruby

* https://www.ruby-lang.org/en/documentation/installation/

4o Passo: Navegar pelo terminal para o diretório do GameLibrary

5o Passo: No terminal rode os seguintes comandos

```sh
$ bundle install
```

```sh
$ rake db:migrate
```

```sh
$ rake db:seed
```

```sh
$ rails server
```

6o Passo: Abra o browser no endereço

* http://localhost:3000