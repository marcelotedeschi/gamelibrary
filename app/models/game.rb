class Game < ActiveRecord::Base

  # MODEL
  # t.string :title
  # t.string :platform
  # t.integer :year
  # t.string :gender
  # t.float :price
  # t.integer :rating


  ## VALIDATIONS ##

  # title
  validates_presence_of :title, message: "não pode ser vazio"
  validates_uniqueness_of :title, message: "já existe"

  # platform
  validates_presence_of :platform, message: "não pode ser vazio"

  # year
  validates_presence_of :year, message: "não pode ser vazio"
  validates_numericality_of :year, only_integer: true, message: "deve conter apenas números"
  validates_length_of :year,  is: 4, message: "deve possuir exatamente 4 digitos"

  # gender
  validates_presence_of :gender, message: "não pode ser vazio"

  # price
  validates_presence_of :price, message: "não pode ser vazio"

  # rating
  validates_presence_of :rating, message: "não pode ser vazio"
  validates_numericality_of :rating, only_integer: true, less_than: 6, message: "deve possuir valor até 5"

end
