class GamesController < ApplicationController
  include ActionView::Helpers::TextHelper

  def index
    @game = Game.all
    @gamesPrice = Game.sum(:price)
    @games_grid = initialize_grid(Game,
                                  order:           'games.rating',
                                  order_direction: 'desc',
                                  per_page: 5
    )
  end

  def new
    @game = Game.new
  end

  def show
    @game = Game.find(params[:id])
  end

  def create
    @game = Game.new(game_params)    # Not the final implementation!
    if @game.save
      # Handle a successful save.
      redirect_to games_path
    elsif @game.errors.any?
      render 'new'
    end
  end

  # definimos game_params caso precisemos restringir certas rows de serem inseridas via http
  private
  def game_params
    params.require(:game).permit(:title, :platform, :gender, :year, :price, :rating)
  end

end
