class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.string :title
      t.string :platform
      t.string :gender
      t.integer :year
      t.float :price
      t.integer :rating

      t.timestamps
    end
  end
end
