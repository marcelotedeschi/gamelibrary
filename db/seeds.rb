# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


Game.create(title: 'CS:GO', platform: 'PC', year: 2014, gender: 'FPS', price: 30.0, rating: 5 )
Game.create(title: 'The Sims 3', platform: 'PC', year: 1930, gender: 'Life Simulator', price: 100, rating: 3 )
Game.create(title: 'Dark Souls', platform: 'PS4', year: 2013, gender: 'Action', price: 40.0, rating: 4 )
Game.create(title: 'LOL', platform: 'PC', year: 2010, gender: 'MOBA', price: 0, rating: 4 )
Game.create(title: 'Final Fantasy X', platform: 'PS2', year: 2010, gender: 'RPG', price: 40.30, rating: 2 )
Game.create(title: 'Starcraft 2', platform: 'PC', year: 2013, gender: 'Strategy', price: 70.20, rating: 4 )
Game.create(title: 'Pac-man', platform: 'Any', year: 1900, gender: 'Arcade', price: 12.0, rating: 5 )